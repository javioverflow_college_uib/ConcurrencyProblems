import time

class Work:
    def __init__(self, countAmountPerWork, startingCount = 0, startingTurn = 0):
        self.count = startingCount
        self.turn = startingTurn
        self.states = {0 : False, 1 : False}
        self.timeLogs = {0 : [], 1: []}
        self.COUNT_AMOUNT_PER_WORK = countAmountPerWork

    def doSomeWork(self, me):
        for i in range(self.COUNT_AMOUNT_PER_WORK):
            self.incrementCount(me)

    def incrementCount(self, me):
        self.preProtocol(me)
        self.criticalSection(me)
        self.postProtocol(me)

    def preProtocol(self, me):
        him = (me + 1) % 2
        self.states[me] = True
        self.turn = him
        while self.states[him] and self.turn == him:
            pass

    def criticalSection(self, me):
        self.count += 1
        self.registerTime(me)

    def postProtocol(self, me):
        self.states[me] = False

    def registerTime(self, me):
        timeLog = time.clock() * (10**9)
        self.timeLogs[me] += [timeLog]
        print "La puerta {}: {} entradas de {} -- Tiempo: {}".format(
                me, 
                len(self.timeLogs[me]),
                len(self.timeLogs[me]) + len(self.timeLogs[(me + 1) % 2]),
                timeLog
            )

    def printAverageTimes(self):
        for identifier, timeLogs in self.timeLogs.items():
            logs = self.timeLogs[identifier]
            totalTime = 0
            for x, y in zip(logs[:-1], logs[1:]):
                totalTime += (y-x)
            averageTime = totalTime / float(len(logs) - 1)
            print "Tiempo medio de la puerta {}: {}".format(identifier, averageTime)

    def printTotalCount(self):
        print "Entradas totales: {}".format(self.count)

