from SomeThreads import SomeThreads
from Work import Work

THREADS_AMOUNT = 2
COUNT_AMOUNT_PER_THREAD = 2000

def main():
    work = Work(COUNT_AMOUNT_PER_THREAD)
    BunchOfThreads = SomeThreads()

    BunchOfThreads.createThreads(work.doSomeWork, THREADS_AMOUNT)
    BunchOfThreads.startThreads()
    BunchOfThreads.joinThreads()

    work.printAverageTimes()
    work.printTotalCount()

if __name__ == "__main__":
    main()
