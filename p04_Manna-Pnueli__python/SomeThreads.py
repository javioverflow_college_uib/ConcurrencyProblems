from threading import Thread

class SomeThreads():

    def createThreads(self, worker, amount):
        self.threads = [Thread(target=worker, args=(i,)) for i in range(amount)]

    def startThreads(self):
        for thread in self.threads:
            thread.start()

    def joinThreads(self):
        for thread in self.threads:
            thread.join()


