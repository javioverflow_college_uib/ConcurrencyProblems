class Work:
    def __init__(self, countAmountPerWork, startingCount = 0):
        self.count = startingCount
        self.want = {0: 0, 1: 0}
        self.COUNT_AMOUNT_PER_WORK = countAmountPerWork

    def doSomeWork(self, me):
        for i in range(self.COUNT_AMOUNT_PER_WORK):
            self.preProtocol(me)
            self.criticalSection()
            self.postProtocol(me)

    def preProtocol(self, me):
        him = (me + 1) % 2
        if me == 0:
            self.want[me] = -1 if self.want[him] == -1 else 1
            while self.want[me] == self.want[him]:
                pass
        elif me == 1:
            self.want[me] = 1 if self.want[him] == -1 else -1
            while self.want[him] == -self.want[me]:
                pass

    def criticalSection(self):
        self.count += 1
        print self.count

    def postProtocol(self, me):
        self.want[me] = 0

