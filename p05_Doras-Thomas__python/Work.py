class Work:
    def __init__(self, countAmountPerWork, startingCount = 0):
        self.count = startingCount
        self.want = {0: 0, 1: 0}
        self.turn = 0
        self.COUNT_AMOUNT_PER_WORK = countAmountPerWork

    def doSomeWork(self, me):
        for i in range(self.COUNT_AMOUNT_PER_WORK):
            self.preProtocol(me)
            self.criticalSection()
            self.postProtocol(me)

    def preProtocol(self, me):
        him = (me + 1) % 2
        self.want[me] = True
        if self.want[him]:
            if self.turn == him:
                self.want[me] = False
                while self.turn != me:
                    pass
                self.want[me] = True
            while self.want[him] != False:
                pass

    def criticalSection(self):
        self.count += 1
        print self.count

    def postProtocol(self, me):
        him = (me + 1) % 2
        self.want[me] = False
        self.turn = him
