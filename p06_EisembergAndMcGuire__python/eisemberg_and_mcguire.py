# @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)
# @source: http://lcsee.wvu.edu/~jdmooney/classes/cs550/notes/tech/mutex/Eisenberg.html

from threading import Thread

THREADS_AMOUNT = 2
MAX_COUNT_AMOUNT = 10000
COUNT_PER_THREAD = MAX_COUNT_AMOUNT/THREADS_AMOUNT

WAITING = 0
ACTIVE = 1
IDLE = 2

status = [WAITING] * THREADS_AMOUNT
turn = 0

count = 0


class Worker(Thread):

    turn = 0
    want_status = [False, False]

    def __init__(self, id):
        self.id = id
        Thread.__init__(self)

    def run(self):
        for count in range(COUNT_PER_THREAD):
            self.pre_protocol()
            self.critical_section()
            self.post_protocol()

    def pre_protocol(self):
        global turn, status, WAITING, ACTIVE, IDLE
        while True:
            status[self.id] = WAITING
            index = turn
            while index != self.id:
                index = turn if status[index] != IDLE else (index + 1) % THREADS_AMOUNT

            status[self.id] = ACTIVE
            index = 0
            while index < THREADS_AMOUNT and (index == self.id or status[index] != ACTIVE):
                index += 1

            if index >= THREADS_AMOUNT and (turn == self.id or status[turn] == IDLE):
                break

        turn = self.id

    def critical_section(self):
        global count
        count += 1

    def post_protocol(self):
        global turn, status, WAITING, ACTIVE, IDLE
        index = (turn + 1) % THREADS_AMOUNT
        while status[index] == IDLE:
            index = (index + 1) % THREADS_AMOUNT

        turn = index
        status[self.id] = IDLE


threads = []


def main():
    create_threads()
    start_threads()
    join_threads()
    output_result()
    
    
def create_threads():
    for id in range(THREADS_AMOUNT):
        new_thread = Worker(id)
        threads.append(new_thread)
    
    
def start_threads():
    for thread in threads:
        thread.start()


def join_threads():
    for thread in threads:
        thread.join()


def output_result():
    print("Counter value: {} -- Expected: {}".format(count, MAX_COUNT_AMOUNT))

if __name__ == "__main__":
    main()