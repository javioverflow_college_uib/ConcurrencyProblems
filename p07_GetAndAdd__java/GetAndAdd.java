// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

package ConcurrencyProblems.p7_GetAndAdd__java;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class Worker implements Runnable {

    static final int MAX_COUNT_AMOUNT = 10_000_000;
    static final int THREAD_AMOUNT = 4;
    static final int COUNT_PER_THREAD = MAX_COUNT_AMOUNT/THREAD_AMOUNT;

    static volatile int count = 0;
    private static AtomicInteger number = new AtomicInteger(0);
    private static AtomicInteger ticket = new AtomicInteger(0);

    @Override
    public void run() {
        for (int count = 0; count<COUNT_PER_THREAD; count++) {
            preProtocol();
            criticalSection();
            postProtocol();
        }

    }

    private void preProtocol() {
        int current = ticket.getAndAdd(1);
        while (current != number.get());
    }

    private void criticalSection() {
        count++;
    }

    private void postProtocol() {
        number.getAndAdd(1);
    }

}

public class GetAndAdd {

    static List<Thread> threads = new ArrayList<Thread>(Worker.THREAD_AMOUNT);

    public static void main(String[] args) throws InterruptedException {
        createThreads();
        startThreads();
        joinThreads();
        outputResult();
    }

    static void createThreads() {
        for (int i=0; i<Worker.THREAD_AMOUNT; i++) {
            Thread new_thread = new Thread(new Worker());
            threads.add(new_thread);
        }
    }

    static void startThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    static void joinThreads() throws InterruptedException{
        for (Thread thread : threads) {
            thread.join();
        }
    }

    static void outputResult() {
        System.out.printf("Count value: %d -- Expected: %d", Worker.count, Worker.MAX_COUNT_AMOUNT);
    }
}