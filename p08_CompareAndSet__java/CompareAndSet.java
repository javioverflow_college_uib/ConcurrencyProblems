// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

package ConcurrencyProblems.p8_CompareAndSet__java;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class Worker implements Runnable {

    static final int MAX_COUNT_AMOUNT = 10_000_000;
    static final int THREAD_AMOUNT = 4;
    static final int COUNT_PER_THREAD = MAX_COUNT_AMOUNT/THREAD_AMOUNT;

    static volatile int count = 0;
    private static AtomicBoolean mutex= new AtomicBoolean(false);

    @Override
    public void run() {
        for (int count = 0; count<COUNT_PER_THREAD; count++) {
            preProtocol();
            criticalSection();
            postProtocol();
        }

    }

    private void preProtocol() {
        while (! mutex.compareAndSet(false, true));
    }

    private void criticalSection() {
        count++;
    }

    private void postProtocol() {
        mutex.set(false);
    }

}

public class CompareAndSet {

    static List<Thread> threads = new ArrayList<Thread>(Worker.THREAD_AMOUNT);

    public static void main(String[] args) throws InterruptedException {
        createThreads();
        startThreads();
        joinThreads();
        outputResult();
    }

    static void createThreads() {
        for (int i=0; i<Worker.THREAD_AMOUNT; i++) {
            Thread new_thread = new Thread(new Worker());
            threads.add(new_thread);
        }
    }

    static void startThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    static void joinThreads() throws InterruptedException{
        for (Thread thread : threads) {
            thread.join();
        }
    }

    static void outputResult() {
        System.out.printf("Count value: %d -- Expected: %d", Worker.count, Worker.MAX_COUNT_AMOUNT);
    }
}