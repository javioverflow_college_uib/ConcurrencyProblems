// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

package main

import (
	"fmt"
	"runtime"
	"sync/atomic"
)

const (
	PROCS_AMOUNT = 4
	THREAD_AMOUNT = 8
	TOTAL_COUNT_AMOUNT = 10000000
	COUNT_PER_THREAD = (TOTAL_COUNT_AMOUNT / THREAD_AMOUNT)
)

var (
	count int = 0
	mutex int32 = 0
)


func main() {
	runtime.GOMAXPROCS(PROCS_AMOUNT)

	done := make(chan bool, 1)
	createThreads(done)
	waitThreads(done)
	outputResult()
}

func createThreads(done chan bool){
	for i := 0; i < THREAD_AMOUNT; i++ {
		go worker(done)
	}
}

func worker(done chan bool) {
	for i := 0; i < COUNT_PER_THREAD; i++ {
		preProtocol()
		criticalSection()
		postProtocol()
	}
	done <- true
}

func preProtocol() {
	for !atomic.CompareAndSwapInt32(&mutex, 0, 1) { }
}

func criticalSection() {
	count++
}

func postProtocol() {
	mutex = 0
}

func waitThreads(done chan bool) {
	for i := 0; i < THREAD_AMOUNT; i++ {
		<-done
	}
}

func outputResult() {
	fmt.Printf("Count value: %d -- Expected: %d", count, TOTAL_COUNT_AMOUNT)
}
