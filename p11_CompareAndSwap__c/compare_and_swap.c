// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

#include "stdio.h"
#include "pthread.h"

#define THREAD_AMOUNT 4
#define MAX_COUNT_AMOUNT 10000000
#define COUNT_PER_THREAD (MAX_COUNT_AMOUNT/THREAD_AMOUNT)


int mutex = 0;
int count = 0;


void pre_protocol() {
    while (! __sync_bool_compare_and_swap(&mutex, 0, 1));
}


void critical_section() {
    ++count;
}


void post_protocol() {
    mutex = 0;
}


void *worker() {

    for (int i = 0; i < COUNT_PER_THREAD; i++) {
        pre_protocol();
        critical_section();
        post_protocol();
    }

    return NULL;
}


void create_threads(pthread_t *threads) {

    for (int i = 0; i < THREAD_AMOUNT; i++)
        pthread_create(&threads[i], NULL, worker, NULL);

}


void join_threads(pthread_t *threads) {

    for (int i = 0; i < THREAD_AMOUNT; i++)
        pthread_join(threads[i], NULL);

}


void output_result() {
    printf("Count value: %d -- Expected: %d", count, MAX_COUNT_AMOUNT);
}


int main() {

    pthread_t threads[THREAD_AMOUNT];

    create_threads(threads);
    join_threads(threads);
    output_result();

    return 0;

}
