// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

package ConcurrencyProblems.p13_ProducerConsumerSemaphores__java;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Semaphore;

class ProducerConsumer implements Runnable {

    class Product {}

    static final int MAX_COUNT_AMOUNT = 10_000;
    static final int THREAD_AMOUNT = 10;
    static final int COUNT_PER_THREAD = MAX_COUNT_AMOUNT/THREAD_AMOUNT;

    static Queue<Product> buffer = new LinkedList<Product>();

    static Semaphore bufferSemaphore = new Semaphore(0);
    static Semaphore entrySemaphore = new Semaphore(1);

    private int id;

    public ProducerConsumer(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for (int count = 0; count<COUNT_PER_THREAD; count++) {
            preProtocol();
            criticalSection();
            postProtocol();
        }

    }

    private void preProtocol() {
        try {
            if (!this.isProducer()) {
                bufferSemaphore.acquire();
            }
            entrySemaphore.acquire();
        } catch (InterruptedException e) {}
    }

    private void criticalSection() {
        if (this.isProducer()) {
            this.produce();
        } else {
            this.consume();
        }
    }

    private void produce() {
        buffer.add(new Product());
        System.out.printf("++ Producer (ID=%d) adds 1 to buffer. Total: %d\n", this.id, buffer.size());
    }

    private void consume() {
        buffer.remove();
        System.out.printf("-- Consumer (ID=%d) removes 1 from buffer. Total: %d\n", this.id, buffer.size());
    }

    private void postProtocol() {
        entrySemaphore.release();
        if (this.isProducer()) {
            bufferSemaphore.release();
        }
    }

    private boolean isProducer() {
        return this.id % 2 == 0;
    }

}

public class ProducerConsumerSemaphores {

    static List<Thread> threads = new ArrayList<Thread>(ProducerConsumer.THREAD_AMOUNT);

    public static void main(String[] args) throws InterruptedException {
        createThreads();
        startThreads();
        joinThreads();
    }

    static void createThreads() {
        for (int id = 0; id < ProducerConsumer.THREAD_AMOUNT; id++) {
            Thread new_thread = new Thread(new ProducerConsumer(id));
            threads.add(new_thread);
        }
    }

    static void startThreads() {
        for (Thread thread : threads) {
            thread.start();
        }
    }

    static void joinThreads() throws InterruptedException{
        for (Thread thread : threads) {
            thread.join();
        }
    }
}