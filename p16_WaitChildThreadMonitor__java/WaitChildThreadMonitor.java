// @author: Javier Ramos (https://gitlab.com/u/JaviOverflow)

// Main thread creates child thread. Parent needs some work from child, but
// not all of it, so parent must wait until child finish that work (child
// will keep doing some other work).

package ConcurrencyProblems.p16_WaitChildThreadMonitor__java;

public class WaitChildThreadMonitor {

    public static final int TOTAL_COUNT_AMOUNT = 1_000;
    static int count = 0;

    class Child extends Thread {

        private boolean workDone;

        @Override
        public void run() {
            work();
        }

        public synchronized void work() {

            workDone = false;
            for (int i = 0; i < TOTAL_COUNT_AMOUNT; i++)
                ++WaitChildThreadMonitor.count;
            workDone = true;

            this.notify();

        }

        public synchronized void waitForWork() throws InterruptedException {
            if (workDone != true) {
                this.wait();
            }
        }
    }

    void main() throws InterruptedException {

        Child child = new Child();

        child.start();
        child.waitForWork();

        System.out.printf("Count value is %d\n", WaitChildThreadMonitor.count);
        child.join();

    }

    public static void main(String[] args) throws InterruptedException {
        (new WaitChildThreadMonitor()).main();
    }
}
