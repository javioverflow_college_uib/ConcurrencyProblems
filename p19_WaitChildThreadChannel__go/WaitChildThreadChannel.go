package main

import (
	"fmt"
	"runtime"
)

const (
	TOTAL_COUNT_AMOUNT = 1000000000
)

var (
	count = 0
)

type Empty struct {}

func main() {

	workDone := make(chan Empty)

	go work(workDone);
	<-workDone

	fmt.Printf("Count value is %d\n", count)

}

func work(workDone chan Empty) {

	runtime.GOMAXPROCS(2)

	fmt.Printf("I'm the child and I start doing some work\n")
	for i := 0; i < TOTAL_COUNT_AMOUNT; i++ {
		count += 1
	}
	fmt.Printf("I'm the child and I finished work\n")

	workDone <- Empty{}

}
